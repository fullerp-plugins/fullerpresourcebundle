<?php

namespace FullErp\ResourceBundle\Tests\Toggleable\Fixture;

use FullErp\ResourceBundle\Model\ToggleableTrait;

/**
 * Created by PhpStorm.
 * User: Haykel.Brinis
 * Date: 08/06/2022
 * Time: 15:29.
 */
class UsingTrait
{
    use ToggleableTrait;
}
